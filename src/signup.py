import uuid

class accountSignup():
    
    def signup(self):
        selection = ""

        while selection != "q":
            selection = self.promptChoice()

            if selection == "1":
                print("ekki komið")
                break
            if selection == "2":
                self.promptSignup()


    def promptChoice(self):
        print("1. Sign in")
        print("2. Sign up")
        print("q. To quit")
        return input("What is your choice? ")


    def promptSignup(self):
        canContinue = False
        # Asks the user for information, and error checks
        while not canContinue:
            # Ask for information about user, some variables are optional
            ID = uuid.uuid4().hex
            First_name = input("First name *optional*: ")          
            Last_name = input("Last name *optional*: ")
            phone_number = input("Phone number *optional*: ")
            # Asks the user for username and error checks
            Username = self.promptUsername()
            # Asks the user for password and error checks
            password = self.promptPassword()
            # Asks the user for email and error checks
            Email = self.promptEmail()
            # if no errors in creating account, Turns True and creates account
            canContinue = True
        print("Account created")
        

    def promptUsername(self):
        canContinue = False
        while not canContinue:
            Username = input("Select a username: ")
            # If the username is shorter then 2 letters, asks the user again
            if not len(Username) > 2:
                print("Usernames must be atleast 3 letters")
                continue
            # Here will be a function to loop through usernames and see if the username is already taken
            # If username is already take, asks the user for another name
            # Havent decided how to store the data yet, so no function for now
            else:
                canContinue = True
        return Username
        
    def promptEmail(self):
        canContinue = False

        while not canContinue:
            email = input("Email address: ")
            try:
                # splits the email at @, if there is no @ in the email, prompts error and asks again
                splitEmailString = email.split("@")
            except:
                print("Email not valid")
                continue

            try:
                # After split the email should be of lengt 2
                not len(splitEmailString[0]) or not len(splitEmailString[1])
            except:
                print("Email not valid")
                continue
            # Checks to see if email after split is of lenght 2
            if len(splitEmailString) != 2:
                print("Email not valid")
                continue
            
            # Grabs the part of the email after the split at @
            secondPart = splitEmailString[1]

            try:
                # Splits the second part of the email after @ on the dot "."
                second_part_list = secondPart.split(".")
            except:
                print("Email not valid")
                continue
            # Checks if after split on dot the email is of max lengt 2 or 3
            if len(second_part_list) not in [2,3]:
                print("Email not valid")
                continue

            else:
                # If everthing is correct, returns the email
                canContinue = True

            if not canContinue:
                print("Email not Valid")
        return email
    
    def promptPassword(self):
        canContinue = False
        
        while not canContinue:
            # Asks the user for a password for his account
            password = input("Select a password: ")
            # Asks the user to repeat his password
            password2 = input("Repeat password: ")
            # Checks to see if the passwords match, if the dont,
            # Asks the user for the passwords again
            if not password == password2:
                print("Password not the same ")
                continue
            else:
                # If both passwords match, returns password
                canContinue = True
        return password


panda = accountSignup()
panda.signup()