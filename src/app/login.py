import csv	
class Login:
    def __init__(self, username, password):
        self.username = username
        self.password = password
        self.logged_in = False

    def get_all_employees_dict(self):
        #Get the userinfo in a list
        with open("users.csv", "r", encoding="UTF-8", newline="") as filestream:
            next(filestream)
            csv_reader = csv.reader(filestream, delimiter=";")
            self.all_users = [line for line in csv_reader]
        return self.all_users

    def validate_credentials(self):
        all_employees = self.get_all_employees_dict()
        #Check if any userinfo matches the input
        for line in all_employees:
            if line[3] == self.username and line[4] == self.password:
                self.logged_in = True
                return True
        return False

    def main(self):
        pass

    def login(self):
        pass

    def create(self):
        pass
