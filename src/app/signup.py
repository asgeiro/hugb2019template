import csv
from login import Login
class SignUp:
    def __init__(self, first_name, last_name, phone, username, password, email):
        self.first_name = first_name
        self.last_name = last_name
        self.phone = phone
        self.username = username
        self.password = password
        self.email = email
        self.user_info = []
        self.isAdmin = 0
        self.user_id = self.getUserID()
        self.all_users = self.getUsers()
        self.createUser()

    def createUser(self):
        self.user_info = [
            self.first_name, 
            self.last_name, 
            self.phone,
            self.username,
            self.password,
            self.email,
            self.isAdmin,
            self.user_id
            ]
        #self.addUser()
    
    def addUser(self):
        if self.distinctUsername():
            self.all_users.append(self.user_info)
            self.writeUsers()
        else:
            print("Username Exists")
            return False

    def writeUsers(self):
        with open("users.csv", "w", encoding="UTF-8", newline="") as filestream:
            csv_writer = csv.writer(filestream, delimiter=";")
            for line in self.all_users:
                csv_writer.writerow(line)
        filestream.close()

    def distinctUsername(self):
        #Check if the username is distinct
        for i in self.all_users:
            if self.username == i[3]:
                return False
        return True

    def getUsers(self):
        with open("users.csv", "r", encoding="UTF-8", newline="") as filestream:
            csv_reader = csv.reader(filestream, delimiter=";")
            all_users = [line for line in csv_reader]
        return all_users

    def getUserID(self):
        return 0


#new = SignUp("asi", "ola", "1", "asio", "bla", "asi@asi.is")