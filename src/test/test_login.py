import unittest
from login import Login
class TestLogin(unittest.TestCase):
    def setUp(self):
        #Create instances
        self.log_test_true = Login("asgeiro", "123")
        self.log_test_false = Login("Username", "Password")
        self.length_of_list = 2

    def test_login_true(self):
        print("Testing valid credentials.")
        #Check if valid credentials are recognized
        validate_credentials_true =  self.log_test_true.validate_credentials()
        self.assertTrue(validate_credentials_true)

    def test_login_false(self):
        print("Testing invalid credentials.")
        #Check if invalid credentials are disallowed
        validate_credentials_false = self.log_test_false.validate_credentials()
        self.assertTrue(not(validate_credentials_false))

    def test_users(self):
        print("Testing total users.")
        #Check if the amount of users are the same as the amount of dummy users 
        user_list = self.log_test_true.get_all_employees_dict()
        self.assertEqual(self.length_of_list, len(user_list))

    def test_first_user(self):
        print("Testing first user.")
        #Check if the first user is the same as the first dummy user
        all_users = self.log_test_true.get_all_employees_dict()
        self.assertEqual(self.log_test_true.username,all_users[0][3])
        self.assertEqual(self.log_test_true.password, all_users[0][4])

    def tearDown(self):
        print("Test done.")

if __name__ == "__main__":
    unittest.main()

