import unittest
from signup import SignUp

class TestSignup(unittest.TestCase):

    def setUp(self):

       self.instance1 = SignUp("asi", "ola", "1", "asio", "bla", "asi@asi.is")
       self.instance2 = SignUp("einhver", "annar", "1", "asgeiro", "bla", "einhver@annar.is")
       self.instance3 = SignUp("thessi", "er", "1", "ekki", "til", "ekki@til.is")
    

    def testCreate(self):
        print("Create")
        self.assertTrue(len(self.instance1.user_info) == 8)
    def testDuplicates(self):
        print("Duplicates")
        self.assertTrue(self.instance3.distinctUsername())
        self.assertTrue(not(self.instance2.distinctUsername()))
    def testGetUsers(self):
        self.assertEqual(list, type(self.instance1.getUsers()))
    def testWriteUsers(self):
        users = self.instance1.getUsers()
        self.assertTrue(self.instance1.user_info not in users)
        self.instance1.writeUsers

    def tearDown(self):
        print("Test done")


if __name__ == "__main__":
    unittest.main()

